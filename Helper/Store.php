<?php

declare(strict_types=1);

namespace Agrekom\Core\Helper;

class Store extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $localeResolver;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\Resolver $localeResolver,
        \Psr\Log\LoggerInterface $logger
    )
    {
        parent::__construct($context);

        $this->storeManager = $storeManager;
        $this->localeResolver = $localeResolver;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getStoreCode()
    {
        $storeCode = 'pl_pl';
        try {
            $storeCode = $this->storeManager->getStore()->getCode();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $noSuchEntityException) {
            $this->logger->error($noSuchEntityException->getMessage());
        }

        return $storeCode;
    }

    /**
     * @return string
     */
    public function getStoreLocaleCode()
    {
        return $this->localeResolver->getLocale();
    }

}
