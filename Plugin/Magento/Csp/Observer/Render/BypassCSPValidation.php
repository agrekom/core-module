<?php

namespace Agrekom\Core\Plugin\Magento\Csp\Observer\Render;

class ByPassCSPValidation
{

    public function aroundExecute(\Magento\Csp\Observer\Render $subject, callable $proceed)
    {
        # Do nothing
    }

}
