require(['jquery'], function ($) {
    $(document).ready(function () {
        var navbar = document.getElementById("main-navbar");
        if (navbar != null) {
            var sticky = navbar.offsetTop;

            window.onscroll = function () {
                stickyNavBar()
            };

            function stickyNavBar() {
                var navigationLogo = document.getElementById("navigation-logo");
                if (window.pageYOffset >= sticky) {
                    navigationLogo.style.display = 'block';
                    navbar.classList.add("sticky-navbar");
                } else {
                    navigationLogo.style.display = 'none';
                    navbar.classList.remove("sticky-navbar");
                }
            }
        }

        $(".cms-home div.cs-container--image-teaser-legacy:eq(1)").addClass("agrekom-container--image-teaser-legacy");
        $(".cms-home div.cs-container--image-teaser-legacy:eq(2)").addClass("agrekom-container--image-teaser-legacy");
        $(".cms-home div.cs-container--image-teaser-legacy:eq(3)").addClass("agrekom-container--image-teaser-legacy");
    });
});
